﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace SeleniumDotNet.Pages
{
    public class ChemAppsProduction
    {
        [FindsBy(How = How.Name, Using = "MSDCMax")]
        [CacheLookup]
        public IWebElement MSDCMax;

        [FindsBy(How = How.Name, Using = "MSDCUnit")]
        [CacheLookup]
        public IWebElement MSDCUnit;

        [FindsBy(How = How.XPath, Using = "/html/body/main/ui-view/ui-view/reliability-footer/footer/div/div/div/button[2]")]
        [CacheLookup]
        public IWebElement SaveAndNext;

        [FindsBy(How = How.XPath, Using = "/html/body/main/ui-view/ui-view/main/section/div/div/div[2]/div[1]/div[3]/div/div/div")]
        [CacheLookup]
        public IWebElement MAPCG;        
    }
}
