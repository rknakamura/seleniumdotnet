﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeleniumDotNet.Framework.Statements;
using SeleniumDotNet.Pages;

namespace SeleniumDotNet
{
    [TestClass]
    public class SeleniumWebTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            Given.Website("http://chemappsdev.na.xom.com/#/reliability/corporate-plan/423/production")
                .When<ChemAppsProduction>(x => x.MSDCMax).Clear(1000).And.SetValue("321")
                .When<ChemAppsProduction>(x => x.MSDCUnit).Clear(1000).And.SetValue("123")
                .Then<ChemAppsProduction>(x => x.MAPCG).Text.ToBe("43.1 kTons")
                .Then<ChemAppsProduction>(x => x.MSDCMax).Value.ToBe("321")
                .Then<ChemAppsProduction>(x => x.MSDCUnit).Value.ToBe("123");
        }
    }
}
