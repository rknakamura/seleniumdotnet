﻿using OpenQA.Selenium;
using SeleniumDotNet.Framework.Actions;
using SeleniumDotNet.Framework.Utils;
using System;
using System.Linq.Expressions;

namespace SeleniumDotNet.Framework.Statements
{
    public abstract class Expectation 
    {
        public PropertyAction Then<T>(Expression<Func<T, IWebElement>> element) where T : class, new()
        {
            var webComponent = LoadWebComponent(element);
            return new PropertyAction(webComponent);
        }

        protected WebComponent LoadWebComponent<T>(Expression<Func<T, IWebElement>> element) where T : class, new()
        {
            var page = WebPage.Load<T>();
            return new WebComponent
            {
                Name = ((MemberExpression)element.Body).Member.Name,
                WebElement = element.Compile().Invoke(page)
            };
        }
    }
}
