﻿using OpenQA.Selenium;
using SeleniumDotNet.Framework.Actions;
using System;
using System.Linq.Expressions;

namespace SeleniumDotNet.Framework.Statements
{
    public class Behavior : Expectation
    {
        public BehaviorAction When<T>(Expression<Func<T, IWebElement>> element) where T : class, new()
        {
            var webComponent = LoadWebComponent(element);
            return new BehaviorAction(webComponent);
        }        
    }
}
