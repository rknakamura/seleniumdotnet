﻿using SeleniumDotNet.Framework.Actions;
using SeleniumDotNet.Framework.Utils;

namespace SeleniumDotNet.Framework.Statements
{
    public class BehaviorConnector : Behavior
    {
        private WebComponent _element;

        public BehaviorConnector(WebComponent element)
        {
            _element = element;
        }

        public BehaviorAction And => new BehaviorAction(_element);
    }
}
