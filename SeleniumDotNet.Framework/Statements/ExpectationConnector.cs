﻿using SeleniumDotNet.Framework.Actions;
using SeleniumDotNet.Framework.Utils;

namespace SeleniumDotNet.Framework.Statements
{
    public class ExpectationConnector : Expectation
    {
        private WebComponent _element;

        public ExpectationConnector(WebComponent element)
        {
            _element = element;
        }

        public PropertyAction And => new PropertyAction(_element);
    }
}
