﻿using SeleniumDotNet.Framework.Utils;
using System;

namespace SeleniumDotNet.Framework.Statements
{
    public class Given
    {
        public static Func<string, Behavior> Website = (url) =>
        {
            WebDriver.Driver.Url = url;
            return new Behavior();
        };
    }
}
