﻿using SeleniumExtras.PageObjects;

namespace SeleniumDotNet.Framework.Utils
{
    public class WebPage 
    {
        public static T Load<T>() where T : class, new()
        {
            var page = new T();
            PageFactory.InitElements(WebDriver.Driver, page);

            return page;
        }
    }
}
