﻿namespace SeleniumDotNet.Framework.Utils
{
    public abstract class CoreAction
    {
        protected string GetPropertyReference(Property property, WebComponent element)
        {
            switch (property)
            {
                case Property.Text:
                    return element.WebElement.Text;

                default:
                    return element.WebElement.GetAttribute(property.ToString().ToLower());
            }
        }
    }
}
