﻿using OpenQA.Selenium;

namespace SeleniumDotNet.Framework.Utils
{
    public class WebComponent
    {
        public string Name { get; set; }
        public IWebElement WebElement { get; set; }
    }
}
