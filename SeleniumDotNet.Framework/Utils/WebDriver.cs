﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace SeleniumDotNet.Framework.Utils
{
    public static class WebDriver
    {
        private const string pathDriver =  @"..\..\..\packages\Selenium.Chrome.WebDriver.2.43\driver";
        private static IWebDriver _driver;

        public static IWebDriver Driver => GetDriver();

        private static IWebDriver GetDriver()
        {
            var chromeOption = new ChromeOptions();
            chromeOption.AddArguments("--disable-extensions");
            chromeOption.AddArguments("--disable-infobars");
            chromeOption.AddAdditionalCapability("useAutomationExtension", false);

            if (_driver == null)
                _driver = new ChromeDriver(pathDriver, chromeOption);

            return _driver;
        }

    }
}
