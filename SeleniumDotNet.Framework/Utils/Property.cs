﻿namespace SeleniumDotNet.Framework.Utils
{
    public enum Property
    {
        None,
        Text,
        Value,
        Title
    }
}
