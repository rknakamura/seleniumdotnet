﻿using SeleniumDotNet.Framework.Statements;
using SeleniumDotNet.Framework.Utils;
using System;
using System.Threading;

namespace SeleniumDotNet.Framework.Actions
{
    public class ExpectationAction : CoreAction
    {
        private WebComponent _element;
        private Property _property;

        public ExpectationAction(WebComponent element, Property property)
        {
            _element = element;
            _property = property;
        }

        public ExpectationConnector ToBe(string value)
            => VerifyEquality(value);

        private ExpectationConnector VerifyEquality(string value)
        {
            Thread.Sleep(500);

            var pageElement = GetPropertyReference(_property, _element);

            if (!pageElement.Equals(value))
                throw new Exception($"{_element.Name} - Expected: {value} But was: {_element.WebElement.GetAttribute("value")}");

            return new ExpectationConnector(_element);
        }
    }
}
