﻿using SeleniumDotNet.Framework.Statements;
using SeleniumDotNet.Framework.Utils;
using System.Threading;

namespace SeleniumDotNet.Framework.Actions
{
    public class BehaviorAction
    {
        private WebComponent _element;
        private BehaviorConnector _connector;

        public BehaviorAction(WebComponent element)
        {
            _element = element;
            _connector = new BehaviorConnector(element);
        }

        public BehaviorConnector Click()
        {
            _element.WebElement.Click();
            return _connector;
        }        

        public BehaviorConnector Click(int timeout)
        {
            Thread.Sleep(timeout);
            return Click();
        }

        public BehaviorConnector Clear()
        {
            _element.WebElement.Clear();
            return _connector;
        }

        public BehaviorConnector Clear(int timeout)
        {
            Thread.Sleep(timeout);
            return Clear();
        }

        public BehaviorConnector SetValue(string value)
        {
            _element.WebElement.SendKeys(value);
            return _connector;
        }

        public BehaviorConnector SetValue(string value, int timeout)
        {
            Thread.Sleep(timeout);
            return SetValue(value);
        }        
    }
}
