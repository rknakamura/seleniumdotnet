﻿using SeleniumDotNet.Framework.Utils;
using System;

namespace SeleniumDotNet.Framework.Actions
{
    public class PropertyAction : CoreAction 
    {
        private WebComponent _element;

        public PropertyAction(WebComponent element)
        {
            _element = element;
        }

        public ExpectationAction Text =>
             CheckAttribute(Property.Text);

        public ExpectationAction Value =>
            CheckAttribute(Property.Value);

        public ExpectationAction Title =>
            CheckAttribute(Property.Title);

        private ExpectationAction CheckAttribute(Property property)
        {
            var element = GetPropertyReference(property, _element);

            if (element == null)
                    throw new Exception($"Element { _element.Name } does not contains property { property.ToString() }");

            return new ExpectationAction(_element, property);
        }        
    }
}
